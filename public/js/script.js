function checkAll(source) {

    checkboxes = document.getElementsByClassName('cocher');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }

}

function ajoutMot(select) {

    var selectedTxt = select.options[select.selectedIndex].text;
    var selectedId = select.options[select.selectedIndex].value;

    var newDiv = document.createElement('div');
    var newWord = document.createElement('span');
    var newAddon = document.createElement('span');
    var newInput = document.createElement('input');

    newDiv.setAttribute('class', 'input-group');
    newDiv.setAttribute('id', 'div_' + selectedId);


    newWord.innerHTML = selectedTxt;
    newWord.setAttribute('id', selectedId);
    newWord.setAttribute('class', 'form-control');
    newWord.setAttribute('id', 'mot_' + selectedId);


    newAddon.innerHTML = '<i class="glyphicon glyphicon-minus btn btn-danger"></i>'
    newAddon.setAttribute('class', 'input-group-btn');
    newAddon.setAttribute('onclick', 'suppMot(' + selectedId + ')');
    newAddon.setAttribute('id', 'addon_' + selectedId);


    newInput.name = "mots[]";
    newInput.setAttribute('type', 'checkbox');
    newInput.setAttribute('checked', 'checked');
    newInput.setAttribute('value', selectedId);
    newInput.setAttribute('id', 'input_' + selectedId);
    newInput.style.display = 'none';

    document.getElementById('nouveauxMots').appendChild(newDiv);
    newDiv.appendChild(newWord);
    newDiv.appendChild(newAddon);
    newDiv.appendChild(newInput);

    select.options[select.selectedIndex].remove();

}

function suppMot(idmot) {

    var newOption = document.createElement('option');
    var mot = document.getElementById('mot_'+idmot);
    newOption.innerHTML = mot.innerHTML;
    newOption.setAttribute('value', idmot);
    document.getElementById('input_' + idmot).remove();
    document.getElementById('addon_' + idmot).remove();
    document.getElementById('mot_' + idmot).remove();
    document.getElementById('div_' + idmot).remove();

    document.getElementById('form_vocabulaire').appendChild(newOption);

}

function showVoc(key, button) {
    var voc = document.getElementById("voc_" + key);
    if (voc.style.display == 'none') {
        voc.removeAttribute('style');
        button.innerHTML = '<span class="glyphicon glyphicon-eye-close"></span>';
    } else {
        voc.style.display = 'none';
        button.innerHTML = '<span class="glyphicon glyphicon-eye-open"></span>';
    }
}