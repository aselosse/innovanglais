<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CategorieController extends AbstractController {

    /**
     * @Route("/categorie", name="categorie")
     */
    public function index(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $categorie = new Categorie();

        $repository = $this->getDoctrine()->getManager()->getRepository(Categorie::class);

        $formAjout = $this->createFormBuilder($categorie)
                ->add('libelle', TextType::class, array('attr' => array('placeholder' => 'Nouvelle catégorie')))
                ->getForm();

        $formSupp = $this->createFormBuilder($categorie)
                ->getForm();

        if ($request->isMethod('POST')) {

            if (isset($request->get('form')['ajout'])) {
                $formAjout->handleRequest($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($categorie);
                $em->flush();
            } else if (isset($request->get('form')['supp'])) {
                $formSupp->handleRequest($request);
                $cocher = $request->request->get('cocher');
                if (isset($cocher)) {

                    foreach ($cocher as $i) {
                        if ($i != 0) {
                            $u = $repository->find($i);
                            $this->getDoctrine()->getManager()->remove($u);
                        }
                    }
                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }

        $listeCategories = $repository->findAll();

        return $this->render('categorie/index.html.twig', ['formAjout' => $formAjout->createView(), 'formSupp' => $formSupp->createView(), 'listeCategories' => $listeCategories]);
    }

    /**
     * @Route("/categorie_modifier/{id}", name="categorie_modifier")
     */
    public function modifier(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $repository = $this->getDoctrine()->getManager()->getRepository(Categorie::class);
        $categorie = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($categorie)
                ->add('libelle', TextType::class)
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-success'), 'label' => 'Modifier'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($categorie);
                $em->flush();
            }
        }
        return $this->render('categorie/modifier.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/categoriews", name="categoriews")
     */
    public function listeCateg() {
        $repository = $this->getDoctrine()->getManager()->getRepository(Categorie::class);
        $list = $repository->findCateg();
        return $this->json($list);
    }

}
