<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Vocabulaire;
use App\Entity\Categorie;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Request;

class VocabulaireController extends AbstractController {

    /**
     * @Route("/vocabulaire", name="vocabulaire")
     */
    public function index(Request $request) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $vocabulaire = new Vocabulaire();
        $repository = $this->getDoctrine()->getManager()->getRepository(Vocabulaire::class);
        $formAjout = $this->createFormBuilder($vocabulaire)
                ->add('motFR', TextType::class, array('attr' => array('placeholder' => 'Mot français'), 'data' => null))
                ->add('motEN', TextType::class, array('attr' => array('placeholder' => 'Traduction anglais'), 'data' => null))
                ->add('categorie', EntityType::class, array('class' => Categorie::class, 'choice_label' => 'libelle'))
                ->getForm();

        $formSupp = $this->createFormBuilder($vocabulaire)
                ->getForm();

        if ($request->isMethod('POST')) {

            if (isset($request->get('form')['ajout'])) {
                $formAjout->handleRequest($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($vocabulaire);
                $em->flush();
            } else if (isset($request->get('form')['supp'])) {
                $formSupp->handleRequest($request);
                $cocher = $request->request->get('cocher');
                if (isset($cocher)) {
                    foreach ($cocher as $i) {
                        if ($i != 0) {
                            $u = $repository->find($i);
                            $this->getDoctrine()->getManager()->remove($u);
                        }
                    }
                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }

        $listeVocabulaire = $repository->findAll();

        return $this->render('vocabulaire/index.html.twig', ['formAjout' => $formAjout->createView(), 'formSupp' => $formSupp->createView(), 'listeVocabulaire' => $listeVocabulaire]);
    }
    
    /**
     * @Route("/vocabulaire_modifier/{id}", name="vocabulaire_modifier")
     */
    public function modifier(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $repository = $this->getDoctrine()->getManager()->getRepository(Vocabulaire::class);
        $vocabulaire = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($vocabulaire)
                ->add('motFR', TextType::class)
                ->add('motEN', TextType::class)
                ->add('categorie', EntityType::class, array('class' => Categorie::class, 'choice_label' => 'libelle'))
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-success'), 'label' => 'Modifier'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($vocabulaire);
                $em->flush();
            }
        }
        return $this->render('vocabulaire/modifier.html.twig', ['form' => $form->createView()]);
    }

}
