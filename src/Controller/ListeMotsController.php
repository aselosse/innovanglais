<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Vocabulaire;
use App\Entity\Theme;
use App\Entity\ListeMots;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;

class ListeMotsController extends AbstractController {

    /**
     * @Route("/liste/mots", name="liste_mots")
     */
    public function index(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $listeMots = new ListeMots();
        $repository = $this->getDoctrine()->getManager()->getRepository(ListeMots::class);
        $repositoryV = $this->getDoctrine()->getManager()->getRepository(Vocabulaire::class);
        $repositoryT = $this->getDoctrine()->getManager()->getRepository(Theme::class);

        $formAjout = $this->createFormBuilder($listeMots)
                ->add('theme', EntityType::class, array('class' => Theme::class, 'choice_label' => 'libelle', 'attr' => array('class' => 'form-control')))
                ->add('niveau', IntegerType::class, array('attr' => array('max' => '10', 'min' => '0', 'class' => 'form-control'), 'label' => 'Niveau', 'data' => 1))
                ->add('vocabulaire', EntityType::class, array('class' => Vocabulaire::class, 'choice_label' => 'mot_fr', 'mapped' => 'false', 'attr' => array('class' => 'form-control')))
                ->getForm();

        $formSupp = $this->createFormBuilder($listeMots)
                ->getForm();

        if ($request->isMethod('POST')) {
            if (isset($request->get('form')['ajout'])) {
                $mots = $request->request->get('mots');
                if (isset($mots)) {
                    foreach ($mots as $i) {
                        $mot = $repositoryV->find($i);
                        $listeMots->addVocabulaire($mot);
                    }
                    $listeMots->setNiveau($_POST['form']['niveau']);
                    $thm = $repositoryT->find($_POST['form']['theme']);
                    $listeMots->setTheme($thm);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($listeMots);
                    $em->flush();
                } 
            } else if (isset($request->get('form')['supp'])) {
                $formSupp->handleRequest($request);
                $cocher = $request->request->get('cocher');
                if (isset($cocher)) {
                    foreach ($cocher as $i) {
                        if ($i != 0) {
                            $u = $repository->find($i);
                            $this->getDoctrine()->getManager()->remove($u);
                        }
                    }
                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }


        $listeDeslistesMots = $repository->findAll();

        return $this->render('liste_mots/index.html.twig', ['formAjout' => $formAjout->createView(), 'formSupp' => $formSupp->createView(), 'listeDeslistesMots' => $listeDeslistesMots
        ]);
    }

    /**
     * @Route("/liste/motWS", name="liste_motWS")
     */
    public function jsonWS() {

        $repository = $this->getDoctrine()->getManager()->getRepository(ListeMots::class);
        $listeId = $repository->findList();
        $listejson = array();
        foreach($listeId as $i=>$s) {
            
            $listejson[$i]['id'] = $listeId[$i]['idList'];
            $listejson[$i]['theme'] = $listeId[$i]['libelle'];
            
            $listejson[$i]['mots'] = $repository->findWordList($listeId[$i]['idList']);
            
        }
        return $this->json($listejson);
    }

}
