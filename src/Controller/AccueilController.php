<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccueilController extends AbstractController {

    /**
     * @Route("/accueil", name="accueil")
     */
    public function index() {
        return $this->render('accueil/index.html.twig', []);
    }

    /**
     * @Route("/inscrire", name="inscrire")
     */
    public function inscrire(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $user = new User();
        $form = $this->createFormBuilder($user)
                ->add('email', TextType::class, array('attr' => array('placeholder' => 'Email', 'class' => 'form-control')))
                ->add('password', PasswordType::class, array('attr' => array('placeholder' => 'Mot de passe', 'class' => 'form-control')))
                ->add('nom', TextType::class, array('attr' => array('placeholder' => 'Nom', 'class' => 'form-control')))
                ->add('prenom', TextType::class, array('attr' => array('placeholder' => 'Prénom', 'class' => 'form-control')))
                ->add('dateNaissance', BirthdayType::class, array('attr' => array('class' => 'form-control', 'style' => 'padding-top:4px')))
                ->getForm();


        if ($request->isMethod('POST')) {
            if (isset($request->get('form')['inscrire'])) {
                $form->handleRequest($request);
                $em = $this->getDoctrine()->getManager();
                $user->setRoles(array("ROLE_USER"));
                $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
                $user->setDateInscription(new \DateTime("now"));
                $user->setEntreprise(null);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('accueil');
            }
        }
        return $this->render('accueil/inscrire.html.twig', ['form' => $form->createView()]);
    }

}
