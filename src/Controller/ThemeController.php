<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Theme;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ThemeController extends AbstractController {

    /**
     * @Route("/theme", name="theme")
     */
    public function index(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');


        $theme = new Theme();

        $repository = $this->getDoctrine()->getManager()->getRepository(Theme::class);

        $formAjout = $this->createFormBuilder($theme)
                ->add('libelle', TextType::class, array('attr' => array('placeholder' => 'Nouveau thème')))
                ->getForm();

        $formSupp = $this->createFormBuilder($theme)
                ->getForm();

        if ($request->isMethod('POST')) {

            if (isset($request->get('form')['ajout'])) {
                $formAjout->handleRequest($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                $em->flush();
            } else if (isset($request->get('form')['supp'])) {
                $formSupp->handleRequest($request);
                $cocher = $request->request->get('cocher');
                if (isset($cocher)) {
                    foreach ($cocher as $i) {
                        if ($i != 0) {
                            $u = $repository->find($i);
                            $this->getDoctrine()->getManager()->remove($u);
                        }
                    }
                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }

        $listeThemes = $repository->findAll();

        return $this->render('theme/index.html.twig', ['listeThemes' => $listeThemes, 'formAjout' => $formAjout->createView(), 'formSupp' => $formSupp->createView()]);
    }

    /**
     * @Route("/theme_modifier/{id}", name="theme_modifier")
     */
    public function modifier(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $repository = $this->getDoctrine()->getManager()->getRepository(Theme::class);
        $theme = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($theme)
                ->add('libelle', TextType::class)
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-success'), 'label' => 'Modifier'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                $em->flush();
            }
        }
        return $this->render('theme/modifier.html.twig', ['form' => $form->createView()]);
    }

}
