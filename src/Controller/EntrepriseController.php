<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Entreprise;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EntrepriseController extends AbstractController {

    /**
     * @Route("/entreprise", name="entreprise")
     */
    public function index(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $entreprise = new Entreprise();
        
        $repository = $this->getDoctrine()->getManager()->getRepository(Entreprise::class);
        
        $formAjout = $this->createFormBuilder($entreprise)
                ->add('denomination', TextType::class, array('attr' => array('placeholder' => 'Dénomination')))
                ->add('telephone', TextType::class, array('attr' => array('placeholder' => 'N° téléphone')))
                ->getForm();

        $formSupp = $this->createFormBuilder($entreprise)
                ->getForm();
        
        if ($request->isMethod('POST')) {

            if (isset($request->get('form')['ajout'])) {
                $formAjout->handleRequest($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entreprise);
                $em->flush();
                
            } else if (isset($request->get('form')['supp'])) {
                $formSupp->handleRequest($request);
                $cocher = $request->request->get('cocher');
                if (isset($cocher)) {
                    foreach ($cocher as $i) {
                        if ($i != 0) {
                            $u = $repository->find($i);
                            $this->getDoctrine()->getManager()->remove($u);
                        }
                    }
                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }
        

        
        $listeEntreprises = $repository->findAll();

        return $this->render('entreprise/index.html.twig', ['formAjout' => $formAjout->createView(), 'formSupp' => $formSupp->createView(), 'listeEntreprises' => $listeEntreprises]);
    }

    /**
     * @Route("/entreprise_modifier/{id}", name="entreprise_modifier")
     */
    public function modifier(Request $request) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        
        $repository = $this->getDoctrine()->getManager()->getRepository(Entreprise::class);
        $entreprise = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($entreprise)
                ->add('denomination', TextType::class)
                ->add('telephone', TextType::class)
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-success'), 'label' => 'Modifier'))
                ->getForm();
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entreprise);
                $em->flush();
            }
        }
        return $this->render('entreprise/modifier.html.twig', ['form' => $form->createView()]);
    }
    
    /**
     * @Route("/entreprisews", name="entreprisews")
     */
    public function listeEntreprise() {
        $repository = $this->getDoctrine()->getManager()->getRepository(Entreprise::class);
        $list = $repository->findEntreprise();
        return $this->json($list);
    }

}
