<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181210125234 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE theme (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE theme_liste_mots (theme_id INT NOT NULL, liste_mots_id INT NOT NULL, INDEX IDX_9B4C1C3F59027487 (theme_id), INDEX IDX_9B4C1C3FD34D82DA (liste_mots_id), PRIMARY KEY(theme_id, liste_mots_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vocabulaire (id INT AUTO_INCREMENT NOT NULL, categorie_id INT DEFAULT NULL, mot_fr VARCHAR(255) NOT NULL, mot_en VARCHAR(255) NOT NULL, INDEX IDX_DB1ADE7DBCF5E72D (categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_mots (id INT AUTO_INCREMENT NOT NULL, niveau INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_mots_vocabulaire (liste_mots_id INT NOT NULL, vocabulaire_id INT NOT NULL, INDEX IDX_844044B0D34D82DA (liste_mots_id), INDEX IDX_844044B0D8B12F03 (vocabulaire_id), PRIMARY KEY(liste_mots_id, vocabulaire_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE theme_liste_mots ADD CONSTRAINT FK_9B4C1C3F59027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE theme_liste_mots ADD CONSTRAINT FK_9B4C1C3FD34D82DA FOREIGN KEY (liste_mots_id) REFERENCES liste_mots (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vocabulaire ADD CONSTRAINT FK_DB1ADE7DBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE liste_mots_vocabulaire ADD CONSTRAINT FK_844044B0D34D82DA FOREIGN KEY (liste_mots_id) REFERENCES liste_mots (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_mots_vocabulaire ADD CONSTRAINT FK_844044B0D8B12F03 FOREIGN KEY (vocabulaire_id) REFERENCES vocabulaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE resultat CHANGE user_id user_id INT DEFAULT NULL, CHANGE test_id test_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE test ADD theme_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0C59027487 FOREIGN KEY (theme_id) REFERENCES theme (id)');
        $this->addSql('CREATE INDEX IDX_D87F7E0C59027487 ON test (theme_id)');
        $this->addSql('ALTER TABLE user CHANGE abonnement_id abonnement_id INT DEFAULT NULL, CHANGE entreprise_id entreprise_id INT DEFAULT NULL, CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE theme_liste_mots DROP FOREIGN KEY FK_9B4C1C3F59027487');
        $this->addSql('ALTER TABLE test DROP FOREIGN KEY FK_D87F7E0C59027487');
        $this->addSql('ALTER TABLE liste_mots_vocabulaire DROP FOREIGN KEY FK_844044B0D8B12F03');
        $this->addSql('ALTER TABLE theme_liste_mots DROP FOREIGN KEY FK_9B4C1C3FD34D82DA');
        $this->addSql('ALTER TABLE liste_mots_vocabulaire DROP FOREIGN KEY FK_844044B0D34D82DA');
        $this->addSql('ALTER TABLE vocabulaire DROP FOREIGN KEY FK_DB1ADE7DBCF5E72D');
        $this->addSql('DROP TABLE theme');
        $this->addSql('DROP TABLE theme_liste_mots');
        $this->addSql('DROP TABLE vocabulaire');
        $this->addSql('DROP TABLE liste_mots');
        $this->addSql('DROP TABLE liste_mots_vocabulaire');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('ALTER TABLE resultat CHANGE user_id user_id INT DEFAULT NULL, CHANGE test_id test_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_D87F7E0C59027487 ON test');
        $this->addSql('ALTER TABLE test DROP theme_id');
        $this->addSql('ALTER TABLE user CHANGE abonnement_id abonnement_id INT DEFAULT NULL, CHANGE entreprise_id entreprise_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
