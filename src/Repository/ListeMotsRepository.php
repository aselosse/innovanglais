<?php

namespace App\Repository;

use App\Entity\ListeMots;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ListeMots|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeMots|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeMots[]    findAll()
 * @method ListeMots[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeMotsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ListeMots::class);
    }

    // /**
    //  * @return ListeMots[] Returns an array of ListeMots objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListeMots
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findList()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = ' SELECT liste_mots.id AS idList, theme.libelle
                FROM liste_mots INNER JOIN theme ON theme_id = theme.id';
        $exec = $conn->prepare($sql); 	 	
        $exec->execute();
        return $exec->fetchAll();
    }
    
    public function findWordList($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT vocabulaire.mot_fr,vocabulaire.mot_en
                FROM `liste_mots_vocabulaire` INNER JOIN liste_mots ON liste_mots.id = liste_mots_id
                                              INNER JOIN vocabulaire ON vocabulaire.id = vocabulaire_id
                WHERE liste_mots.id = :id ';
        $exec = $conn->prepare($sql); 	 	
        $exec->execute(array(':id'=>$id));
        return $exec->fetchAll();
    }
    
}
