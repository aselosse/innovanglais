<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VocabulaireRepository")
 */
class Vocabulaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motEN;
    
    
    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="Vocabulaire")
    */
    private $categorie; 

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMotFR(): ?string
    {
        return $this->motFR;
    }

    public function setMotFR(string $motFR): self
    {
        $this->motFR = $motFR;

        return $this;
    }

    public function getMotEN(): ?string
    {
        return $this->motEN;
    }

    public function setMotEN(string $motEN): self
    {
        $this->motEN = $motEN;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }
}
