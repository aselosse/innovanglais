<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListeMotsRepository")
 */
class ListeMots
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $niveau;
    
    /**
    * @ORM\ManyToMany(targetEntity="App\Entity\Vocabulaire")
    */
    private $vocabulaire;
    
    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Theme", inversedBy="theme")
    */
    private $theme;

    public function __construct()
    {
        $this->vocabulaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function setNiveau(int $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * @return Collection|Vocabulaire[]
     */
    public function getVocabulaire(): Collection
    {
        return $this->vocabulaire;
    }

    public function addVocabulaire(Vocabulaire $vocabulaire): self
    {
        if (!$this->vocabulaire->contains($vocabulaire)) {
            $this->vocabulaire[] = $vocabulaire;
        }

        return $this;
    }

    public function removeVocabulaire(Vocabulaire $vocabulaire): self
    {
        if ($this->vocabulaire->contains($vocabulaire)) {
            $this->vocabulaire->removeElement($vocabulaire);
        }

        return $this;
    }

    public function getTheme(): ?Theme
    {
        return $this->theme;
    }

    public function setTheme(?Theme $theme): self
    {
        $this->theme = $theme;

        return $this;
    }
}
