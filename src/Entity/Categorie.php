<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;
    
    /**
    * @ORM\OneToMany(targetEntity="App\Entity\Vocabulaire", mappedBy="Categorie")
    */
    private $vocabulaire;

    public function __construct()
    {
        $this->vocabulaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Vocabulaire[]
     */
    public function getVocabulaire(): Collection
    {
        return $this->vocabulaire;
    }

    public function addVocabulaire(Vocabulaire $vocabulaire): self
    {
        if (!$this->vocabulaire->contains($vocabulaire)) {
            $this->vocabulaire[] = $vocabulaire;
            $vocabulaire->setCategorie($this);
        }

        return $this;
    }

    public function removeVocabulaire(Vocabulaire $vocabulaire): self
    {
        if ($this->vocabulaire->contains($vocabulaire)) {
            $this->vocabulaire->removeElement($vocabulaire);
            // set the owning side to null (unless already changed)
            if ($vocabulaire->getCategorie() === $this) {
                $vocabulaire->setCategorie(null);
            }
        }

        return $this;
    }
}
